#include "boot.h"
#include "usart.h"
#include "delay.h"

load_application p_load_Application;
u32 iapbuf[512]; 	//2K字节缓存  

//MSP重定向
__asm void MSR_SP(u32 addr)
{
    msr msp, r0     //将addr加载进sp指针，r0为函数传入第一个参数，即addr
    bx r14          //跳到上一个函数的地址，r14为连接寄存器，保存上一次的函数的地址
}

//跳转至Application分区
void LOAD_Application(u32 addr)
{
    MSR_SP(*(u32*)addr);
    p_load_Application = (load_application) *(u32*)(addr + 4); //u32* 地址addr强制转化为函数指针
    p_load_Application();//函数指针调用
}

//将程序烧写进flash
void Flash_download(u32 appxaddr,u8 *appbuf,u32 appsize)
{
    u32 t;
	u16 i=0;
	u32 temp;
	u32 fwaddr=appxaddr;//当前写入的地址
	u8 *dfu=appbuf;
	for(t=0;t<appsize;t+=4)
	{
		temp=(u32)dfu[3]<<24;   
		temp|=(u32)dfu[2]<<16;    
		temp|=(u32)dfu[1]<<8;
		temp|=(u32)dfu[0];	  
		dfu+=4;//偏移4个字节
		iapbuf[i++]=temp;	    
		if(i==512)
		{
			i=0; 
			STMFLASH_Write(fwaddr,iapbuf,512);
			fwaddr+=2048;//偏移2048  512*4=2048
		}
	}
	if(i)STMFLASH_Write(fwaddr,iapbuf,i);//将最后的一些内容字节写进去.  
}

void set_bootloader()
{
    u32 temp_data  = 1;
    STMFLASH_Write(bootloader_data_addr, &temp_data, 1);
}

void check_bootloadr()
{
	u32 temp_data;
	temp_data = *(u32*)(bootloader_data_addr);
	if(temp_data) Application_service();
}

//bootloader服务
void Application_service()
{
    LOAD_Application(application_start_addr);
}




