#include "sys.h"
#include "usart.h"	
#include "string.h"
#include "stmflash.h"
#include "boot.h"
#include "delay.h"

//命令存储数组
//命令协议xxxxxxxxx\r\n注意带换行
u8 USART_COMMAND[USART_CLI_NUM+1][SIGAL_USART_CLI_LONG] =
{
    "BOOTLOADER_UNLOCK\r\n",    //bootloader开锁
    "BOOTLOADER_LOCKED\r\n",    //bootloader上锁
	"FLASH_DOWNLOAD\r\n",		//将缓存区的数据烧写进flash
	"LOAD_APPLACATION\r\n",		//程序开始运行
};

//bootloader接收缓冲,最大USART_REC_LEN个字节,起始地址为0X20001000,大小50kb
u8 USART_BOOTLOADER_RX_BUF[USART_REC_LEN] __attribute__ ((at(0X20001000)));
//COMMAND接收缓冲,最大SIGAL_USART_CLI_LONG个字节
u8 USART_COMMAND_RX_BUF[SIGAL_USART_CLI_LONG] = {0};

u32 rx_command_buf_cnt = 0;		//bootloader包计数器
u32 USART_BOOTLOADER_RX_CNT = 0;	//BOOTLODER接收的字节数
u32 usart_time_cnt = 0;				//超时检测

u8 	bootloader_lock = 0;			//bootloader锁 0上锁 1解锁
u8 	USART_RX_STA = 0;				//COMMAND接收的状态
u8 last_rx_buf = 0;					//上一次串口接收到的数据

//加入以下代码,支持printf函数,而不需要选择use MicroLIB	  
#if 1
#pragma import(__use_no_semihosting)             
//标准库需要的支持函数                 
struct __FILE 
{ 
	int handle; 
}; 

FILE __stdout;       
//定义_sys_exit()以避免使用半主机模式    
void _sys_exit(int x)
{ 
	x = x; 
} 
//重定义fputc函数 
int fputc(int ch, FILE *f)
{ 	
	while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
	USART1->DR = (u8) ch;      
	return ch;
}
#endif

//初始化IO 串口1 
//bound:波特率
void uart_init(u32 bound)
{
   //GPIO端口设置
  	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE); //使能GPIOA时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);//使能USART1时钟
 
	//串口1对应引脚复用映射
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource9,GPIO_AF_USART1); //GPIOA9复用为USART1
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource10,GPIO_AF_USART1); //GPIOA10复用为USART1
	
	//USART1端口配置
  	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10; //GPIOA9与GPIOA10
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;//复用功能
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;	//速度50MHz
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP; //推挽复用输出
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP; //上拉
	GPIO_Init(GPIOA,&GPIO_InitStructure); //初始化PA9，PA10

   //USART1 初始化设置
	USART_InitStructure.USART_BaudRate = bound;//波特率设置
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;//字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;//一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;//无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//收发模式
  	USART_Init(USART1, &USART_InitStructure); //初始化串口1
	
  	USART_Cmd(USART1, ENABLE);  //使能串口1 
	
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);//开启相关中断

	//Usart1 NVIC 配置
  	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;//串口1中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=3;//抢占优先级3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority =3;		//子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器、
}

void USART1_IRQHandler(void)                	//串口1中断服务程序
{
	u8 Res;
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)  //接收中断
	{
		Res = USART_ReceiveData(USART1);	//读取接收到的数据

		if(bootloader_lock)	//OTA数据缓存
		{
			usart_time_cnt = 0;	//超时计数复位
			if(USART_BOOTLOADER_RX_CNT<USART_REC_LEN)
			{
				USART_BOOTLOADER_RX_BUF[USART_BOOTLOADER_RX_CNT] = Res;
				USART_BOOTLOADER_RX_CNT++;			     
			}
			else 	//溢出
			{
				printf("Buff overflow!");
				bootloader_lock = 0;
				return;
			}
		}
		else	//命令数据处理
		{
			USART_COMMAND_RX_BUF[rx_command_buf_cnt] = Res;
			rx_command_buf_cnt ++;
			if((last_rx_buf == 0X0D) && (Res == 0X0A)) USART_RX_STA = 1;// 符号\r\n
			last_rx_buf = Res;
		}
  	}
}

//获取串口命令
//返回值 0:无命令 其他数字:对应命令排序
u32 usart_boot_cli(void)
{
	u32 p_COM_BUF;
	u32 p_COM_CLE_BUF;

	for(p_COM_BUF = 0; p_COM_BUF < USART_CLI_NUM; p_COM_BUF++)		//轮询命令
	{
		if(!strcmp((char *)USART_COMMAND[p_COM_BUF], (char *)USART_COMMAND_RX_BUF)) 
		{
			if(USART_COMMAND_RX_BUF[0] == 0) break;		//空字符不要
			for(p_COM_CLE_BUF = 0 ; p_COM_CLE_BUF <= SIGAL_USART_CLI_LONG; p_COM_CLE_BUF++) 
				USART_COMMAND_RX_BUF[p_COM_CLE_BUF] = 0; //清零操作s
			return p_COM_BUF + 1;//对应命令编号
		}
	}
	return 0;
}

//命令处理函数
void command_execute()
{
	if(USART_RX_STA != 1) return;	//还没接收完命令

	switch (usart_boot_cli())
	{
		case ERROR_COMMAND:
			printf("Command error!");
		break;

		case  BOOTLOADER_UNLOCK: 
			bootloader_lock = 1; 
			printf("Data write unlock\r\nPlease input OTA file");
		break;
			
		case  BOOTLOADER_LOCKED: 
			bootloader_lock = 0; 
			printf("Data write Locked");
		break;

		case FLASH_DOWNLOAD:
			printf("Downloading...");
			Flash_download(application_start_addr, USART_BOOTLOADER_RX_BUF, USART_BOOTLOADER_RX_CNT);
			USART_BOOTLOADER_RX_CNT = bootloader_lock = 0;			//烧写完上锁
		break;

		case LOAD_APPLACATION:
			printf("Run project\r\n");
			set_bootloader();
			delay_ms(1);
			Application_service();
		break;

		default: break;
	}

	printf("   Done!\r\n");

	rx_command_buf_cnt = USART_RX_STA = 0;	
}




