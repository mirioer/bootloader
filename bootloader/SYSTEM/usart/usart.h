#ifndef __USART_H
#define __USART_H

#include "stdio.h"	
#include "stm32f4xx_conf.h"
#include "sys.h" 

//命令编号
#define ERROR_COMMAND         0
#define BOOTLOADER_UNLOCK     1
#define BOOTLOADER_LOCKED     2
#define FLASH_DOWNLOAD        3
#define LOAD_APPLACATION      4

#define USART_REC_LEN  			120*1024  	//定义最大接收字节数 120k
#define USART_CLI_LEN           30      //命令接收最大字节
#define USART_CLI_NUM           10      //串口命令数量
#define SIGAL_USART_CLI_LONG    30      //单个命令最大字符数
	  	
extern u8  USART_BOOTLOADER_RX_BUF[USART_REC_LEN]; //接收缓冲,最大USART_REC_LEN个字节.末字节为换行符 
extern u32 usart_time_cnt;
extern u32 USART_BOOTLOADER_RX_CNT;
extern u8  bootloader_lock;


//如果想串口中断接收，请不要注释以下宏定义
void uart_init(u32 bound);
u32 usart_boot_cli(void);
void command_execute(void);

#endif


