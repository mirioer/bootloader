/*
*   广东海洋大学蓝网实验室
*	功能：OTA升级程序bootloader
*   版本v1
*/



#include "delay.h"
#include "usart.h"
#include "boot.h"

u32 last_usart_cnt = 0;

int main()
{
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置系统中断优先级分组2
	delay_init(168);		//延时初始化
	uart_init(115200);	//串口初始化波特率为115200

	check_bootloadr();
	
	printf("-------------bootloader-------------\r\n");

	while(1)
	{
		//传输烧录数据
		if(bootloader_lock)
		{
			//超时检测
			usart_time_cnt++;
			if(usart_time_cnt > 50)
			{
				usart_time_cnt = bootloader_lock = 0;//5s超时锁定
				printf("Time out!\r\n");
			}

			//判断传输状态
			if(USART_BOOTLOADER_RX_CNT)
			{
				if(last_usart_cnt == USART_BOOTLOADER_RX_CNT)
				{
					last_usart_cnt = 0;
					printf("Write data finish, code size %d Bytes\r\n", USART_BOOTLOADER_RX_CNT);
					bootloader_lock = 0;
				}
				else 
				{
					last_usart_cnt = USART_BOOTLOADER_RX_CNT;
					printf("Write data %d Bytes\r\n", USART_BOOTLOADER_RX_CNT);
				}
			}
		}
		
		command_execute();
		delay_ms(100);
	}
}





