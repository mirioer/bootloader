#ifndef __BOOT_H
#define __BOOT_H

#include "sys.h"

#define ADDR_FLASH_SECTOR_0     ((u32)0x08000000) 	//扇区0起始地址, 16 Kbytes  
#define ADDR_FLASH_SECTOR_1     ((u32)0x08004000) 	//扇区1起始地址, 16 Kbytes  
#define ADDR_FLASH_SECTOR_2     ((u32)0x08008000) 	//扇区2起始地址, 16 Kbytes  
#define ADDR_FLASH_SECTOR_3     ((u32)0x0800C000) 	//扇区3起始地址, 16 Kbytes  
#define ADDR_FLASH_SECTOR_4     ((u32)0x08010000) 	//扇区4起始地址, 64 Kbytes  
#define ADDR_FLASH_SECTOR_5     ((u32)0x08020000) 	//扇区5起始地址, 128 Kbytes  
#define ADDR_FLASH_SECTOR_6     ((u32)0x08040000) 	//扇区6起始地址, 128 Kbytes  
#define ADDR_FLASH_SECTOR_7     ((u32)0x08060000) 	//扇区7起始地址, 128 Kbytes  
#define ADDR_FLASH_SECTOR_8     ((u32)0x08080000) 	//扇区8起始地址, 128 Kbytes  
#define ADDR_FLASH_SECTOR_9     ((u32)0x080A0000) 	//扇区9起始地址, 128 Kbytes  
#define ADDR_FLASH_SECTOR_10    ((u32)0x080C0000) 	//扇区10起始地址,128 Kbytes  
#define ADDR_FLASH_SECTOR_11    ((u32)0x080E0000) 	//扇区11起始地址,128 Kbytes  


#define bootloader_start_addr   ADDR_FLASH_SECTOR_0   //bootloader起始地址   扇区0
#define bootloader_data_addr    ADDR_FLASH_SECTOR_2   //bootloader配置数据地址   扇区1
#define application_start_addr  ADDR_FLASH_SECTOR_4   //application起始地址  扇区4

typedef void (*load_bootloader)(void);               //app主函数指针


__asm void MSR_SP(u32 addr);
void Flash_download(u32 appxaddr,u8 *appbuf,u32 appsize);
void BootLoader_service(void);
void set_bootloader(void);
void LOAD_BootLoader(u32 addr);

#endif

