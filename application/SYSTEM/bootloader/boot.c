#include "boot.h"
#include "stmflash.h"

load_bootloader p_load_Bootloader; 

//MSP重定向
__asm void MSR_SP(u32 addr)
{
    msr msp, r0     //将addr加载进sp指针，r0为函数传入第一个参数，即addr
    bx r14          //跳到上一个函数的地址，r14为连接寄存器，保存上一次的函数的地址
}

//跳转至Application分区
void LOAD_BootLoader(u32 addr)
{
    MSR_SP(*(u32*)addr);
    p_load_Bootloader = (load_bootloader) *(u32*)(addr + 4); //u32* 地址addr强制转化为函数指针
    p_load_Bootloader();//函数指针调用
}

//bootloader服务
void BootLoader_service()
{
    LOAD_BootLoader(bootloader_start_addr);
}

void set_bootloader()
{
    u32 temp_data  = 0;
    STMFLASH_Write(bootloader_data_addr, &temp_data, 1);
}



