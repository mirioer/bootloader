#ifndef __BOOT_H
#define __BOOT_H

#include "sys.h"

#define bootloader_start_addr   ADDR_FLASH_SECTOR_0   //bootloader起始地址   扇区0
#define application_start_addr  ADDR_FLASH_SECTOR_4    //application起始地址  扇区4

typedef void (*load_application)(void);               //app主函数指针

__asm void MSR_SP(u32 addr);
void Flash_download(u32 appxaddr,u8 *appbuf,u32 appsize);
void Application_service(void);
void LOAD_Application(u32 addr);

#endif

